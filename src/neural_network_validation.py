import pickle
import inspect
from pathlib import Path

import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split

from models import make_embed_dims, get_model

RANDOM_STATE = 42
WORKING_DIR = Path(inspect.getsourcefile(lambda: 0)).resolve().parent.parent
DATA_DIR = WORKING_DIR / 'data'

train = pd.read_csv(DATA_DIR / 'train.csv', index_col='id')
X = train.drop('target', axis=1)
y = train.target.values

df_nunique = X.nunique()
all_names = X.columns
cat_names = [x for x, y in zip(df_nunique.index, df_nunique) if y > 2]
other_names = list(set(all_names).difference(set(cat_names)))
orig_dims = df_nunique[cat_names]
embed_dims = make_embed_dims(orig_dims, max_dim=10)

model = get_model(cat_names, other_names, orig_dims, embed_dims)
model.compile(loss='binary_crossentropy', optimizer='adam')

##############################################################################
# draw the model
##############################################################################
#from tensorflow.keras.utils import plot_model
#plot_model(model, show_shapes=True, to_file='model.png', dpi=192)
##############################################################################

##############################################################################
# Entity Embedding - Preprocessing & Validation
##############################################################################
X_encoded = X.apply(LabelEncoder().fit_transform)
X_tmp, X_test, y_tmp, y_test = train_test_split(
    X_encoded, y, test_size=0.20, random_state=RANDOM_STATE
)
X_train, X_val, y_train, y_val = train_test_split(
    X_tmp, y_tmp, test_size=0.20, random_state=RANDOM_STATE
)

def to_list(x):
    x_list = [x[col].values for col in cat_names]
    x_list.append(
            x[other_names].apply(lambda col: col.astype('float32')).values
    )
    return x_list

X_train_list = to_list(X_train)
X_val_list = to_list(X_val)
X_test_list = to_list(X_test)

model.fit(
    X_train_list, y_train,
    validation_data=(X_val_list, y_val),
    epochs=2,
    batch_size=32,
    verbose=1
)

y_train_pred = model.predict(X_train_list)
y_val_pred = model.predict(X_val_list)
y_test_pred = model.predict(X_test_list)

print('AUC Train:\t', roc_auc_score(y_train, y_train_pred))
print('AUC Val:\t', roc_auc_score(y_val, y_val_pred))
print('AUC Test:\t', roc_auc_score(y_test, y_test_pred))

##############################################################################
# Extracting embedded layers
##############################################################################
embedded_layers_dict = {
        name: model.get_layer(f'{name}_embedded').get_weights()[0]
        for name in cat_names
}
with open('./embedded_layers_dict.pkl', 'wb') as pickle_file:
    pickle.dump(embedded_layers_dict, pickle_file)
##############################################################################

